package mapper;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class QueryTweetsMapper extends MapReduceBase implements Mapper<LongWritable, Text, IntWritable, LongWritable> {

    float UPPER =  40.176632f;
    float LOWER =  40.058223f;
    float LEFT  = -88.365343f;
    float RIGHT = -88.160036f;

    @Override
    public void map(LongWritable key, Text value, OutputCollector<IntWritable, LongWritable> output,
	    Reporter reporter) throws IOException {
		//Split the text input into array
		String[] line = value.toString().split(",");

		//Cast String input to double
		float lat = Float.parseFloat(line[0]);
		float lon = Float.parseFloat(line[1]);

		//Check if the point lies into our bounding box
		if (lat < LOWER || lat > UPPER || lon < LEFT || lon > RIGHT) return;
		
		//Send the (key,value) out
		output.collect(new IntWritable(1), new LongWritable(1));
   }
}
