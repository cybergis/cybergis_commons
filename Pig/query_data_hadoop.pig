REGISTER piggybank.jar;
--Load the data from HDFS and define the schema
data = LOAD '/user/root/2014augtweets.txt' 
USING PigStorage(',') AS (lat:float , lon:float);

-- Filter out for geographical bound
data_filtered = FILTER data BY (lat >= 40.058223 AND lat <= 40.176632 AND lon >= -88.365343 AND lon <= -88.160036);

-- Group all the records
data_group = GROUP data_filtered ALL;

-- Find out count
final_count = FOREACH data_group GENERATE COUNT(data_filtered);

-- Output the result
DUMP final_count
